package com.atlassian.xmlrpc;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.net.MalformedURLException;
import java.net.URL;
import junit.framework.TestCase;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.WebServer;
import org.apache.xmlrpc.server.AbstractReflectiveHandlerMapping.AuthenticationHandler;

public abstract class XmlRpcTestCase extends TestCase
{
    private final String BINDER_CLASS_PROPERTY = "binder.client.test";

    private WebServer server;
    
    protected final int port = 9001;
    
    protected BinderPropertyHandlerMapping handlerMapping;

    @Override
    protected void setUp() throws Exception 
    {
        super.setUp();
        server = new WebServer(port);
        handlerMapping = new BinderPropertyHandlerMapping();
        XmlRpcServer xmlRpcServer = server.getXmlRpcServer();
        xmlRpcServer.setHandlerMapping(handlerMapping);
        xmlRpcServer.setTypeFactory(new BinderTypeFactory(xmlRpcServer));
        
        XmlRpcServerConfigImpl serverConfig = (XmlRpcServerConfigImpl) xmlRpcServer.getConfig();
        serverConfig.setEnabledForExtensions(true);
        serverConfig.setContentLengthOptional(false);
        
        server.start();
    }

    public Binder getBinder()
    {
        if (System.getProperty(BINDER_CLASS_PROPERTY) == null)
        {
            throw new RuntimeException("The system property " + BINDER_CLASS_PROPERTY + " was not specified");
        }

        try
        {
            return (Binder)getClass().getClassLoader().loadClass(System.getProperty(BINDER_CLASS_PROPERTY)).newInstance();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    
    protected URL getServiceUrl() throws MalformedURLException
    {
        return new URL("http://localhost:" + port);
    }

    protected void setAuthenticationHandler(AuthenticationHandler authenticationHandler)
    {
        handlerMapping.setAuthenticationHandler(authenticationHandler);
    }
    
    protected void addHandler(Class serviceObjectType) throws Exception
    {
        if (serviceObjectType == null)
        {
            throw new Exception("serviceObjectType cannot be null");
        }
        
        final String serviceObjectName = getServiceObjectName(serviceObjectType);
        if (serviceObjectName == null)
        {
            fail(serviceObjectType.getName() + " did not implement in interface that was class annotated with the ServiceObject annotation");
        }
        handlerMapping.addHandler(serviceObjectName, serviceObjectType);
    }
    
    protected String getServiceObjectName(Class serviceObjectType) throws Exception
    {
        if (serviceObjectType == null)
        {
            throw new Exception("serviceObjectType cannot be null");
        }
        
        for (Class implementingInterface : serviceObjectType.getInterfaces())
        {
            ServiceObject serviceObject = (ServiceObject)implementingInterface.getAnnotation(ServiceObject.class);
            if (serviceObject != null)
            {
                return serviceObject.value();
            }
        }
        return null;
    }

    @Override
    protected void tearDown()
        throws Exception 
    {
        super.tearDown();
        server.shutdown();
    }
}
