package com.atlassian.xmlrpc;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.ws.commons.util.NamespaceContextImpl;
import org.apache.xmlrpc.common.TypeFactory;
import org.apache.xmlrpc.common.TypeFactoryImpl;
import org.apache.xmlrpc.common.XmlRpcController;
import org.apache.xmlrpc.common.XmlRpcStreamConfig;
import org.apache.xmlrpc.parser.NullParser;
import org.apache.xmlrpc.parser.TypeParser;
import org.apache.xmlrpc.serializer.MapSerializer;
import org.apache.xmlrpc.serializer.NullSerializer;
import org.apache.xmlrpc.serializer.TypeSerializer;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * A ServiceBean aware Apache XML-RPC TypeFactory
 * 
 * @author jdumay
 */
public class BinderTypeFactory extends TypeFactoryImpl 
{
    public BinderTypeFactory(XmlRpcController controller)
    {
        super(controller);
    }

    @Override
    public TypeSerializer getSerializer(XmlRpcStreamConfig pConfig, Object pObject)
        throws SAXException 
    {
        if (isServiceBean(pObject))
        {
            return new BeanSerializer(this, pConfig);
        }
        else
        {
            return super.getSerializer(pConfig, pObject);
        }
    }

    @Override
    public TypeParser getParser(XmlRpcStreamConfig pConfig, NamespaceContextImpl pContext, String pURI, String pLocalName) 
    {
        if (NullSerializer.NIL_TAG.equals(pLocalName)) {
                return new NullParser();
        }
        return super.getParser(pConfig, pContext, pURI, pLocalName);
    }
    
    private boolean isServiceBean(Object object)
    {
        if (object != null && object.getClass() != null)
        {
            return object.getClass().isAnnotationPresent(ServiceBean.class);
        }
        return false;
    }

    /**
     * Converts a bean to a map
     */
    public class BeanSerializer extends MapSerializer
    {
        public BeanSerializer(TypeFactory pTypeFactory, XmlRpcStreamConfig pConfig)
        {
            super(pTypeFactory, pConfig);
        }

        @Override
        protected void writeData(ContentHandler pHandler, Object pData) 
            throws SAXException 
        {
            try
            {
                pData = getMap(pData);
            }
            catch (IllegalAccessException e)
            {
                throw new SAXException(e);
            }
            catch (InvocationTargetException e)
            {
                throw new SAXException(e);
            }
            catch (NoSuchMethodException e)
            {
                throw new SAXException(e);
            }
            super.writeData(pHandler, pData);
        }
        
        private Map getMap(Object object)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
        {
            final Map<String, Object> map = new HashMap<String, Object>();
            for (final PropertyDescriptor descriptor : PropertyUtils.getPropertyDescriptors(object))
            {
                if (descriptor.getReadMethod() != null && descriptor.getWriteMethod() != null)
                {
                    String name = descriptor.getName();
                    final String value = BeanUtils.getProperty(object, name);
                    Method writeMethod = descriptor.getWriteMethod();
                    ServiceBeanField beanField = writeMethod.getAnnotation(ServiceBeanField.class);
                    if (beanField != null && !beanField.value().equals(""))
                    {
                        name = beanField.value();
                    }
                    map.put(name, value);
                }
            }
            return map;
        }
    }
}
