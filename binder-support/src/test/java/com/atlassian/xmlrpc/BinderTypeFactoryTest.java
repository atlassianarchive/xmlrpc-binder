package com.atlassian.xmlrpc;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.io.File;
import java.io.OutputStream;
import org.apache.xml.serialize.XMLSerializer;
import java.util.TimeZone;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.xmlrpc.XmlRpcConfig;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.common.TypeFactory;
import org.apache.xmlrpc.common.XmlRpcController;
import org.apache.xmlrpc.common.XmlRpcStreamConfig;
import org.apache.xmlrpc.common.XmlRpcWorker;
import org.apache.xmlrpc.common.XmlRpcWorkerFactory;
import org.apache.xmlrpc.parser.NullParser;
import org.apache.xmlrpc.parser.TypeParser;
import org.apache.xmlrpc.serializer.NullSerializer;
import org.apache.xmlrpc.serializer.TypeSerializer;

public class BinderTypeFactoryTest extends TestCase
{
    private MockXmlRpcController mockXmlRpcController;
    private BinderTypeFactory binderTypeFactory;
    private MockXmlRpcStreamConfig mockConfig;
    private TestServiceBean serviceBean;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mockXmlRpcController = new MockXmlRpcController();
        binderTypeFactory = new BinderTypeFactory(mockXmlRpcController);
        mockXmlRpcController.setTypeFactory(binderTypeFactory);
        mockConfig = new MockXmlRpcStreamConfig();
        serviceBean = new TestServiceBean();
        serviceBean.setValue("Hello World!");
    }
    
    public void testCanParseNilRpcTypes() throws Exception
    {
        TypeParser parser = binderTypeFactory.getParser(mockConfig, null, null, NullSerializer.NIL_TAG);
        assertNotNull(parser);
        assertTrue(parser instanceof NullParser);
    }
    
    public void testServiceBeanSerializerMapsNamesAndReturnsAValidBeanMap() throws Exception
    {
        File expectedResult = new File("src/test/resources/serialized_bean.txt");
        assertTrue(expectedResult.exists());
        
        File output = File.createTempFile("beantypefactorytest", null, new File("target"));
        XMLSerializer contentHandler = new XMLSerializer();
        OutputStream os = FileUtils.openOutputStream(output);
        contentHandler.setOutputByteStream(os);
        contentHandler.startDocument();
        
        try
        {
            TypeSerializer serializer = binderTypeFactory.getSerializer(mockConfig, serviceBean);
            assertNotNull(serializer);
            assertTrue(serializer instanceof BinderTypeFactory.BeanSerializer);
            serializer.write(contentHandler, serviceBean);
        }
        finally
        {
            IOUtils.closeQuietly(os);
        }
        
        assertTrue("Serialized bean should match bean serialized example", 
                FileUtils.contentEquals(output, expectedResult));
    }
    
    public void testServiceBeanReturnsBeanSerializer() throws Exception
    {
        TypeSerializer serializer = binderTypeFactory.getSerializer(mockConfig, serviceBean);
        assertNotNull(serializer);
        assertTrue(serializer instanceof BinderTypeFactory.BeanSerializer);
    }
    
    private class MockXmlRpcStreamConfig implements XmlRpcStreamConfig
    {
        public String getEncoding() {
            return "UTF-8";
        }

        public TimeZone getTimeZone() {
            return TimeZone.getDefault();
        }

        public boolean isEnabledForExtensions() {
            return true;
        }

        public boolean isUseExtensionsNamespace() {
            return true;
        }
    }
    
    private class MockXmlRpcController extends XmlRpcController
    {
        @Override
        public XmlRpcConfig getConfig() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected XmlRpcWorkerFactory getDefaultXmlRpcWorkerFactory() {
            return new XmlRpcWorkerFactory(mockXmlRpcController) {

                @Override
                protected XmlRpcWorker newWorker() {
                    return new XmlRpcWorker() {

                        public XmlRpcController getController() {
                            return mockXmlRpcController;
                        }

                        public Object execute(XmlRpcRequest pRequest) throws XmlRpcException {
                            return new Object();
                        }
                    };
                };
            };
        }
        
        @Override
        public int getMaxThreads() {
            return super.getMaxThreads();
        }

        @Override
        public TypeFactory getTypeFactory() {
            return super.getTypeFactory();
        }

        @Override
        public XmlRpcWorkerFactory getWorkerFactory() {
            return super.getWorkerFactory();
        }

        @Override
        public void setMaxThreads(int pMaxThreads) {
            super.setMaxThreads(pMaxThreads);
        }

        @Override
        public void setTypeFactory(TypeFactory pTypeFactory) {
            super.setTypeFactory(pTypeFactory);
        }

        @Override
        public void setWorkerFactory(XmlRpcWorkerFactory pFactory) {
            super.setWorkerFactory(pFactory);
        }
    };
    
    @ServiceBean
    public class TestServiceBean
    {
        private String value;

        public String getValue() {
            return value;
        }

        @ServiceBeanField("myvalue")
        public void setValue(String value) {
            this.value = value;
        }
    }
}