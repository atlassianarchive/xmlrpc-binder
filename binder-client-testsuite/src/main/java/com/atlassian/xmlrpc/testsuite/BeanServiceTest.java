package com.atlassian.xmlrpc.testsuite;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import com.atlassian.xmlrpc.XmlRpcTestCase;
import com.atlassian.xmlrpc.Binder;
import com.atlassian.xmlrpc.testsuite.testservices.BeanService;
import com.atlassian.xmlrpc.testsuite.testservices.BeanServiceImpl;
import com.atlassian.xmlrpc.testsuite.testservices.MyBean;

import java.util.List;

public class BeanServiceTest extends XmlRpcTestCase
{
    private BeanService service;
    
    @Override
    protected void setUp()
        throws Exception
    {
        super.setUp();
        addHandler(BeanServiceImpl.class);
        
        Binder binder = getBinder();
        service = binder.bind(BeanService.class, getServiceUrl());
    }
    
    public void testReturnsBeans() throws Exception
    {
        List<MyBean> list  = service.getBeans();
        assertEquals(1, list.size());
        assertEquals("helloworld", list.get(0).getMessage());
    }
    
    public void testReturnsSingleBean() throws Exception
    {
        MyBean bean = service.getBean();
        assertNotNull(bean);
        assertEquals("helloworld", bean.getMessage());
    }
}
