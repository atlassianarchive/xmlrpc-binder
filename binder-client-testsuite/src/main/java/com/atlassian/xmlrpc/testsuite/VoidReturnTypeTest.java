package com.atlassian.xmlrpc.testsuite;

import com.atlassian.xmlrpc.testsuite.testservices.VoidReturnTypeService;
import com.atlassian.xmlrpc.testsuite.testservices.VoidReturnTypeServiceImpl;
import com.atlassian.xmlrpc.XmlRpcTestCase;

public class VoidReturnTypeTest extends XmlRpcTestCase
{
    public void testVoidReturnType() throws Exception
    {
        addHandler(VoidReturnTypeServiceImpl.class);

        VoidReturnTypeService service = getBinder().bind(VoidReturnTypeService.class, getServiceUrl());
        assertFalse(service.hasReturnedVoid());
        service.returnVoid();
        assertTrue(service.hasReturnedVoid());
    }
}
