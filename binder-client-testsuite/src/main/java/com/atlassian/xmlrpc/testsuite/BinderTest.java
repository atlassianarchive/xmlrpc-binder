package com.atlassian.xmlrpc.testsuite;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.net.URL;
import java.util.concurrent.Callable;
import com.atlassian.xmlrpc.Binder;
import com.atlassian.xmlrpc.BindingException;
import com.atlassian.xmlrpc.XmlRpcTestCase;

public class BinderTest extends XmlRpcTestCase
{
    public void testThrowsExceptionIfBinderIsClass()
        throws Exception
    {
        Binder binder = getBinder();
        try
        {
            binder.bind(Object.class, new URL("http://localhost"));
            fail("Should fail as binder type was class");
        }
        catch (BindingException e)
        {
            assertTrue(true);
        }
    }

    public void testThrowsExceptionIfServiceObjectAnnotationIsNotPresent()
        throws Exception
    {
        Binder binder = getBinder();
        try
        {
            binder.bind(Callable.class, new URL("http://localhost"));
            fail("Should fail as Callable does not have ServiceObject annotation");
        }
        catch (BindingException e)
        {
            assertTrue(true);
        }
    }
}
