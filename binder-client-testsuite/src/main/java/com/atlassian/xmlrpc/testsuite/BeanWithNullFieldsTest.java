
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.atlassian.xmlrpc.testsuite;

import com.atlassian.xmlrpc.XmlRpcTestCase;
import com.atlassian.xmlrpc.Binder;
import com.atlassian.xmlrpc.testsuite.testservices.BeanWithNullFieldsServiceImpl;
import com.atlassian.xmlrpc.testsuite.testservices.BeanWithNullFieldsService;
import com.atlassian.xmlrpc.testsuite.testservices.MyBeanWithNullFields;

/**
 *
 * @author jdumay
 */
public class BeanWithNullFieldsTest extends XmlRpcTestCase
{
    public void testBeanWithNullFieldsDoesNotThrowExceptions() throws Exception
    {
        addHandler(BeanWithNullFieldsServiceImpl.class);
        Binder binder = getBinder();
        BeanWithNullFieldsService service = binder.bind(BeanWithNullFieldsService.class, getServiceUrl());
        MyBeanWithNullFields mybean = service.getObject();
        assertNull(mybean.getList());
    }
}
