package com.atlassian.xmlrpc.testsuite;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import com.atlassian.xmlrpc.XmlRpcTestCase;
import com.atlassian.xmlrpc.Binder;
import com.atlassian.xmlrpc.ConnectionInfo;
import com.atlassian.xmlrpc.testsuite.testservices.HelloWorldServiceImpl;
import com.atlassian.xmlrpc.testsuite.testservices.HelloWorldService;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.common.XmlRpcHttpRequestConfig;
import org.apache.xmlrpc.server.AbstractReflectiveHandlerMapping.AuthenticationHandler;

/**
 * @author <a href="mailto:james@atlassian.com">James William Dumay</a>
 */
public class SimpleServiceWithBasicHttpAuthTest extends XmlRpcTestCase
{
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        addHandler(HelloWorldServiceImpl.class);
        setAuthenticationHandler(new AuthenticationHandler() {

            public boolean isAuthorized(XmlRpcRequest pRequest) throws XmlRpcException
            {
                XmlRpcHttpRequestConfig config = (XmlRpcHttpRequestConfig) pRequest.getConfig();
                return config.getBasicUserName().equals("james") && config.getBasicPassword().equals("mypassword");
            }
        });
    }


    public void testSimpleAuth()
        throws Exception
    {
        Binder binder = getBinder();
        ConnectionInfo connectionInfo = new ConnectionInfo();
        connectionInfo.setUsername("james");
        connectionInfo.setPassword("mypassword");
        HelloWorldService service = binder.bind(HelloWorldService.class, getServiceUrl(), connectionInfo);
        String result = service.say("James");
        assertEquals("Hello James", result);
    }

    public void testSimpleAuthWithIncorrectCredentials()
        throws Exception
    {
        Binder binder = getBinder();
        ConnectionInfo connectionInfo = new ConnectionInfo();
        connectionInfo.setUsername("james");
        connectionInfo.setPassword("thewrongpassword");
        HelloWorldService service = binder.bind(HelloWorldService.class, getServiceUrl(), connectionInfo);
        try
        {
            service.say("James");
            fail("Should have thrown an exception");
        }
        catch (Throwable e)
        {
            assertEquals("Could not execute RPC method say", e.getMessage());
        }
    }
}
