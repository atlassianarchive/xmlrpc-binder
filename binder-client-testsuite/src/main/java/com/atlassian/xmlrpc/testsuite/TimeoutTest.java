//package com.atlassian.xmlrpc.testsuite;
//
///*
// * Licensed to the Apache Software Foundation (ASF) under one
// * or more contributor license agreements.  See the NOTICE file
// * distributed with this work for additional information
// * regarding copyright ownership.  The ASF licenses this file
// * to you under the Apache License, Version 2.0 (the
// * "License"); you may not use this file except in compliance
// * with the License.  You may obtain a copy of the License at
// *
// *  http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing,
// * software distributed under the License is distributed on an
// * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// * KIND, either express or implied.  See the License for the
// * specific language governing permissions and limitations
// * under the License.
// */
//
//import com.atlassian.xmlrpc.XmlRpcTestCase;
//import com.atlassian.xmlrpc.ConnectionInfo;
//import com.atlassian.xmlrpc.testsuite.testservices.TimeoutServiceImpl;
//import com.atlassian.xmlrpc.testsuite.testservices.TimeoutService;
//
//import java.net.SocketTimeoutException;
//
///**
// *
// * @author jdumay
// */
//public class TimeoutTest extends XmlRpcTestCase
//{
//    public void testTimesOut() throws Exception
//    {
//        addHandler(TimeoutServiceImpl.class);
//
//        try
//        {
//            ConnectionInfo connectionInfo = new ConnectionInfo();
//            connectionInfo.setTimeout(1000);
//
//            TimeoutService service = getBinder().bind(TimeoutService.class, getServiceUrl(), connectionInfo);
//            service.doTimeout();
//            fail("Should have thrown a timeout exception");
//        }
//        catch (Exception e)
//        {
//            assertTrue(e.getCause().getCause().getClass() == SocketTimeoutException.class);
//        }
//    }
//}
