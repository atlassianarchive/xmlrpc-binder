package com.atlassian.xmlrpc;

import java.util.TimeZone;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * Specifies connection information for the binding.
 * @author jdumay
 */
public final class ConnectionInfo
{
    private final String DEFAULT_ENCODING = "UTF-8";

    private boolean gzip = false;

    private int timeout = 60000;

    private String encoding = DEFAULT_ENCODING;

    private String username;

    private String password;

    private TimeZone timeZone;

    /**
     * Encoding for RPC binding. Default is "UTF-8"
     * @return encoding
     */
    public String getEncoding() {
        return encoding;
    }

    /**
     * Encoding for RPC binding. Default is "UTF-8"
     * @param encoding
     */
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    /**
     * Use Gzip compression
     * @return gzip
     */
    public boolean isGzip() {
        return gzip;
    }

    /**
     * Use Gzip compression
     * @param gzip
     */
    public void setGzip(boolean gzip) {
        this.gzip = gzip;
    }

    /**
     * Username for authentication
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Username for authentication
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Password for authentication
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Password for authentication
     * @param passsword
     */
    public void setPassword(String passsword) {
        this.password = passsword;
    }

    /**
     * Timeout used for RPC communication. The default is 60000 millisconds.
     * @return timeout
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * Timeout used for RPC communication. The default is 60000 millisconds.
     * @param timeout
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * TimeZone
     * @return timezone
     */
    public TimeZone getTimeZone() {
        return timeZone;
    }

    /**
     * TimeZone
     * @param timeZone
     */
    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }
}
