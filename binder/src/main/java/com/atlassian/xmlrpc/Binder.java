package com.atlassian.xmlrpc;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import java.net.URL;

/**
 * Used to bind the given XML-RPC service to an instance of 
 * the given interface type
 * 
 * @author <a href="mailto:james@atlassian.com">James William Dumay</a>
 */
public interface Binder
{
    /**
     * Bind the given type to a RPC service
     * @param <T>
     * @param bindClass
     * @return instance of remote service
     */
    public <T> T bind(Class<T> bindClass, URL url) throws BindingException;

    /**
     * ind the given type to a RPC service given the ConnectionInfo
     * @param <T>
     * @param bindClass
     * @param url
     * @param connectionInfo
     * @return instace of remote service
     * @throws com.atlassian.xmlrpc.BindingException
     */
    public <T> T bind(Class<T> bindClass, URL url, ConnectionInfo connectionInfo) throws BindingException;
}
