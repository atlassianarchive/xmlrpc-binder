package com.atlassian.xmlrpc.spring;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import com.atlassian.xmlrpc.BinderRequestProcessorFactoryFactory;
import com.atlassian.xmlrpc.BinderXmlRpcServlet;
import com.atlassian.xmlrpc.ServiceObject;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.AbstractReflectiveHandlerMapping.AuthenticationHandler;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcHandlerMapping;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Spring enabled BinderXmlRpcServlet
 * 
 * To use, specify a bean that is a list of Binder services and set the servlet
 * parameter "serviceListBeanName" to the name of this bean.
 *
 * The servlet parameter "authHandlerBeanName" specifies the bean to use for
 * a custom AuthenticationHandler instance.
 */
public class BinderSpringXmlRpcServlet extends BinderXmlRpcServlet
{
    public static final String SERVICE_LIST_BEAN_NAME = "serviceListBeanName";

    public static final String AUTH_HANDLER_BEAN_NAME = "authHandlerBeanName";
    
    private ApplicationContext context;

    private List serviceObjects;

    @Override
    public void init(ServletConfig pConfig)
        throws ServletException
    {
        //Hack due to not being able to override handleInitParameters()
        ServletConfig proxiedConfig = getProxyServletConfig(pConfig);
        super.init(proxiedConfig);

        serviceObjects = getServiceObjects(pConfig);
       
	    //Authentication hander must be init before we set the handler mapping 
        initAuthenticationHandler(pConfig);
		initRequestProcessorFactoryFactory();
		
        try
        {
            getXmlRpcServletServer().setHandlerMapping(newXmlRpcHandlerMapping());
        }
        catch (XmlRpcException e)
        {
            throw new ServletException(e.getMessage(), e);
        }
    }
    
    protected ApplicationContext getApplicationContext()
    {
        if (context == null)
        {
            context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        }
        return context;
    }

    public void setApplicationContext(ApplicationContext context)
    {
        this.context = context;
    }
    
    private void initRequestProcessorFactoryFactory() throws ServletException
    {
        setRequestProcessorFactoryFactory(new BinderRequestProcessorFactoryFactory(serviceObjects));
    }

    private void initAuthenticationHandler(ServletConfig pConfig) throws ServletException
    {
        final String beanName = pConfig.getInitParameter(AUTH_HANDLER_BEAN_NAME);
        if (beanName == null)
        {
            return; //authentication handler is optional
        }

        Object o = context.getBean(beanName);
        if (o instanceof AuthenticationHandler)
        {
            setAuthenticationHandler((AuthenticationHandler)o);
        }
        else
        {
            throw new ServletException(beanName + " is supposed to be a implementation of AuthenticationHandler but was " + o.getClass().getName());
        }
    }

    @Override
    protected XmlRpcHandlerMapping newXmlRpcHandlerMapping()
        throws XmlRpcException
    {
        try
        {
            return newPropertyHandlerMapping(null);
        }
        catch (IOException e)
        {
            throw new XmlRpcException("Should never be thrown", e);
        }
    }

    private List getServiceObjects(ServletConfig pConfig) throws ServletException
    {
        final String beanName = pConfig.getInitParameter(SERVICE_LIST_BEAN_NAME);
        if (beanName == null)
        {
            throw new ServletException("Parameter not specified " + SERVICE_LIST_BEAN_NAME);
        }
        
        final List serviceList = (List)getApplicationContext().getBean(beanName);
        if (serviceList == null)
        {
            throw new ServletException("Could not find bean " +  beanName);
        }
        return serviceList;
    }

    @Override
    protected PropertyHandlerMapping newPropertyHandlerMapping(URL url) throws IOException, XmlRpcException {
        PropertyHandlerMapping handlerMapping = super.newPropertyHandlerMapping(url);

        if (serviceObjects != null)
        {
            for (Object service : serviceObjects)
            {
                final String serviceObjectName = findServiceObjectNameForServiceObject(service);
                if (serviceObjectName == null)
                {
                    throw new XmlRpcException("Could not get Service Object name for service " + service.getClass().getName());
                }

                handlerMapping.addHandler(serviceObjectName, service.getClass());
            }
        }

        return handlerMapping;
    }

    private String findServiceObjectNameForServiceObject(Object service)
    {
        String serviceObjectName = null;
        for (Class interfaceType : service.getClass().getInterfaces())
        {
            ServiceObject serviceObject = (ServiceObject)interfaceType.getAnnotation(ServiceObject.class);
            if (serviceObject != null)
            {
                serviceObjectName = serviceObject.value();
            }
        }
        return serviceObjectName;
    }
    
    private ServletConfig getProxyServletConfig(ServletConfig config)
    {
        IgnoreParamsInvocationHandler handler = new IgnoreParamsInvocationHandler(config);
        return (ServletConfig)Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { ServletConfig.class }, handler);
    }
    
    private class IgnoreParamsInvocationHandler implements InvocationHandler
    {
        private final ServletConfig config;

        public IgnoreParamsInvocationHandler(ServletConfig config)
        {
            this.config = config;
        }

        public Object invoke(Object object, Method method, Object[] args)
            throws Throwable
        {
            if (method.getName().equals("getInitParameterNames"))
            {
                Enumeration enumeration = config.getInitParameterNames();
                return new EnumerationWrapper(enumeration);
            }
            return method.invoke(config, args);
        }

        class EnumerationWrapper implements Enumeration
        {
            private final Iterator iterator;

            public EnumerationWrapper(Enumeration enumeration)
            {
                final List<String> elements = new ArrayList<String>();
                do
                {
                    final String element = (String)enumeration.nextElement();
                    if (!SERVICE_LIST_BEAN_NAME.equals(element) && !AUTH_HANDLER_BEAN_NAME.equals(element))
                    {
                        elements.add(element);
                    }
                }
                while (enumeration.hasMoreElements());
                iterator = elements.iterator();
            }

            public boolean hasMoreElements() {
                return iterator.hasNext();
            }

            public Object nextElement() {
                return iterator.next();
            }
        }
    }
}
