package com.atlassian.xmlrpc.spring;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import junit.framework.TestCase;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcHandler;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.XmlRpcRequestConfig;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.AbstractReflectiveHandlerMapping.AuthenticationHandler;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockServletConfig;
import org.springframework.mock.web.MockServletContext;

import com.atlassian.xmlrpc.BinderRequestProcessorFactoryFactory;

public class BinderSpringXmlRpcServletTest extends TestCase
{
    private MockServletConfig config;
    private MockApplicationContext context;
    private static final TestServiceImpl service = new TestServiceImpl();
    private static final XmlRpcRequest request = new XmlRpcRequest() {

        public XmlRpcRequestConfig getConfig() {
            return null;
        }

        public String getMethodName() {
            return "test.ping";
        }

        public int getParameterCount() {
            return 0;
        }

        public Object getParameter(int pIndex) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    };
    
    private static final AuthenticationHandler AUTHENTICATION_HANDLER = new AuthenticationHandler() {

        public boolean isAuthorized(XmlRpcRequest pRequest) throws XmlRpcException {
            return true;
        }
    };
    
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        config = new MockServletConfig(new MockServletContext());
        config.addInitParameter(BinderSpringXmlRpcServlet.SERVICE_LIST_BEAN_NAME, "serviceList");
        config.addInitParameter(BinderSpringXmlRpcServlet.AUTH_HANDLER_BEAN_NAME, "authnHandler");
        
        context = new MockApplicationContext();
        context.beans.put("serviceList", Arrays.asList(service));
        context.beans.put("authnHandler", AUTHENTICATION_HANDLER);
    }

    public void testHandlersAreConstructedFromBeanList() throws Exception
    {
        final BinderSpringXmlRpcServlet servlet = new BinderSpringXmlRpcServlet();

        servlet.setApplicationContext(context);
        servlet.init(config);
        
        assertNotNull(servlet.getXmlRpcServletServer());
        assertNotNull(servlet.getXmlRpcServletServer().getHandlerMapping());

        final XmlRpcHandler handler = servlet.getXmlRpcServletServer().getHandlerMapping().getHandler("test.Ping");

        assertNotNull(handler);
        assertEquals("should have called service handler", "pong", handler.execute(request));

        assertNotNull("Should have a authentication handler instance", servlet.getAuthenticationHandler());
        assertEquals("should be same object", AUTHENTICATION_HANDLER, servlet.getAuthenticationHandler());
    }
    
    public void testIfBinderRequestProcessorFactoryFactoryIsUsed() throws Exception
    {
        final BinderSpringXmlRpcServlet servlet = new BinderSpringXmlRpcServlet();

        servlet.setApplicationContext(context);
        servlet.init(config);
        
        assertNotNull(servlet.getXmlRpcServletServer());
        assertNotNull(servlet.getXmlRpcServletServer().getHandlerMapping());
        
        PropertyHandlerMapping mapping = (PropertyHandlerMapping) servlet.getXmlRpcServletServer().getHandlerMapping();
        assertTrue( mapping.getRequestProcessorFactoryFactory() instanceof BinderRequestProcessorFactoryFactory );        
    }

    public class MockApplicationContext implements ApplicationContext
    {
        public MockApplicationContext()
        {
            beans = new HashMap<String, Object>();
        }

        private final HashMap<String, Object> beans;
        
        public AutowireCapableBeanFactory getAutowireCapableBeanFactory() throws IllegalStateException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getDisplayName() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ApplicationContext getParent() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public long getStartupDate() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean containsBeanDefinition(String beanName) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public int getBeanDefinitionCount() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String[] getBeanDefinitionNames() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String[] getBeanNamesForType(Class type) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String[] getBeanNamesForType(Class type, boolean includePrototypes, boolean allowEagerInit) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Map getBeansOfType(Class type) throws BeansException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Map getBeansOfType(Class type, boolean includePrototypes, boolean allowEagerInit) throws BeansException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean containsBean(String name) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String[] getAliases(String name) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Object getBean(String name) throws BeansException {
            return beans.get(name);
        }

        public Object getBean(String name, Class requiredType) throws BeansException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Object getBean(String name, Object[] args) throws BeansException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Class getType(String name) throws NoSuchBeanDefinitionException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isPrototype(String name) throws NoSuchBeanDefinitionException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isSingleton(String name) throws NoSuchBeanDefinitionException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isTypeMatch(String name, Class targetType) throws NoSuchBeanDefinitionException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean containsLocalBean(String name) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public BeanFactory getParentBeanFactory() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getMessage(String code, Object[] args, Locale locale) throws NoSuchMessageException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void publishEvent(ApplicationEvent event) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Resource[] getResources(String locationPattern) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ClassLoader getClassLoader() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Resource getResource(String location) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
