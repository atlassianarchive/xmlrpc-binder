package com.atlassian.xmlrpc;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.lang.reflect.Method;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.PropertyHandlerMapping;

/**
 * A ServiceObject aware PropertyHandlerMapping implementation.
 * 
 * Remaps the names of service methods specified by the ServiceMethod annotation
 * 
 * @author jdumay
 */
public class BinderPropertyHandlerMapping extends PropertyHandlerMapping
{
    public BinderPropertyHandlerMapping()
    {
        setVoidMethodEnabled(true);
    }

    /**
     * Registers public service methods with the servlet.
     * @param pKey
     * @param pType
     * @throws org.apache.xmlrpc.XmlRpcException
     */
    @Override
    protected void registerPublicMethods(String pKey, Class pType)
        throws XmlRpcException 
    {
        super.registerPublicMethods(pKey, pType);
        
        for (final Method method : pType.getMethods())
        {
            if (!isHandlerMethod(method))
            {
                continue;
            }
            
            ServiceMethod serviceMethod = null;
            for (Class interfaceType : pType.getInterfaces())
            {
                for (final Method methodOnInterface : interfaceType.getMethods())
                {
                    if (methodOnInterface.getName().equals(method.getName()) && compareMethodSignature(method, methodOnInterface))
                    {
                        serviceMethod = methodOnInterface.getAnnotation(ServiceMethod.class);
                        if (serviceMethod != null)
                        {
                            break;
                        }
                    }
                }
                
                if (serviceMethod != null)
                {
                    break;
                }
            }
            
            if (serviceMethod != null && serviceMethod.value() != null)
            {
                String name = pKey + "." + method.getName();
                final Object value = handlerMap.get(name);
                if (value != null)
                {
                    handlerMap.remove(name);
                    name = pKey + "." + serviceMethod.value();
                    handlerMap.put(name, value);
                }
            }
        }
    }

    /**
     * Compares the signatures of two methodss
     * @param method1
     * @param method2
     * @return equality
     */
    private boolean compareMethodSignature(final Method method1, final Method method2)
    {
        final Class[] param1 = method1.getParameterTypes();
        final Class[] param2 = method2.getParameterTypes();
        
        if (param1.length != param2.length)
        {
            return false;
        }
        
        for (int i = 0; i < param1.length; i++)
        {
            if (!param1[i].equals(param2[i]))
            {
                return false;
            }
        }
        
        return true;
    }
}