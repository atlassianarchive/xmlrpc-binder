package com.atlassian.xmlrpc;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.io.IOException;
import java.net.URL;
import java.util.TimeZone;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.common.TypeConverterFactory;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.RequestProcessorFactoryFactory;
import org.apache.xmlrpc.server.XmlRpcHttpServerConfig;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.XmlRpcServlet;
import org.apache.xmlrpc.webserver.XmlRpcServletServer;

/**
 * Subclass of the Apache XmlRpcServlet that uses a BinderPropertyHandlerMapping
 * instead of the regular PropertyHandlerMapping and sets the BinderTypeFactory
 * on the XmlRpcServletServer.
 * 
 * This allows ServiceMethod annotations to remap method names 
 * on the published object and the translation of beans inbound and outbound.
 */
public class BinderXmlRpcServlet extends XmlRpcServlet 
{
    private XmlRpcServletServer server;

    @Override
    public void init(ServletConfig pConfig) throws ServletException
    {
        super.init(pConfig);
    }
    
    @Override
    public XmlRpcServletServer getXmlRpcServletServer() 
    {
        if (server == null)
        {
            server = super.getXmlRpcServletServer();
            server.setTypeFactory(new BinderTypeFactory(server));
            XmlRpcServerConfigImpl config = new XmlRpcServerConfigImpl();
            config.setEnabledForExceptions(true);
            config.setEnabledForExtensions(true);
            server.setConfig(config);
        }
        return server;
    }
    
    @Override
    protected PropertyHandlerMapping newPropertyHandlerMapping(URL url)
        throws IOException, XmlRpcException 
    {
        BinderPropertyHandlerMapping mapping = new BinderPropertyHandlerMapping();
        mapping.setAuthenticationHandler(getAuthenticationHandler());
        
        RequestProcessorFactoryFactory requestProcessorFactoryFactory = getRequestProcessorFactoryFactory();
        if (requestProcessorFactoryFactory != null) 
        {
            mapping.setRequestProcessorFactoryFactory(requestProcessorFactoryFactory);
        }
        
        TypeConverterFactory converterFactory = getTypeConverterFactory();
        if (converterFactory != null) 
        {
            mapping.setTypeConverterFactory(converterFactory);
        }
        else
        {
            mapping.setTypeConverterFactory(getXmlRpcServletServer().getTypeConverterFactory());
        }
        
        if (url != null)
        {
            mapping.load(Thread.currentThread().getContextClassLoader(), url);
        }

        mapping.setVoidMethodEnabled(true);

        return mapping;
    }
}
