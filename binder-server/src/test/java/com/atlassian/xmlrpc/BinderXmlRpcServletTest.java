package com.atlassian.xmlrpc;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.io.File;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;
import org.mortbay.jetty.testing.HttpTester;
import org.mortbay.jetty.testing.ServletTester;

public class BinderXmlRpcServletTest extends TestCase
{
    private static final String methodName = "ping.DoPing";
    
    private ServletTester servletTester;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        servletTester = new ServletTester();
        servletTester.setContextPath("/");
        servletTester.addServlet(BinderXmlRpcServlet.class, "/xmlrpc/");
        servletTester.start();
    }
    
    public void testPingService() throws Exception
    {
        final File result = new File("src/test/resources/expected_result.txt");
        assertTrue(result.exists());
        
        HttpTester response = makeRequestAndReturnResponse();
        assertEquals(200, response.getStatus());
        assertNotNull(response.getContent());
        final String expected = FileUtils.readFileToString(result);

        //Write this out for debugging purposes
        FileUtils.writeStringToFile(new File("target/expected_result_testPingService.txt"), response.getContent().trim());

        assertEquals(expected.trim(), response.getContent().trim());
    }
    
    private HttpTester makeRequestAndReturnResponse() throws Exception
    {
        HttpTester request = new HttpTester();
        HttpTester response = new HttpTester();
        
        request.setMethod("POST");
        request.setURI("/xmlrpc/");
        request.setVersion("HTTP/1.1");
        request.setHeader("Host", "MYTEST");
        request.setHeader("Content-Type", "text/xml");
        request.setHeader("User-Agent", "Binder Servlet Test/1.0");
        request.setContent(getMethodCallForRequestBody(methodName));
        
        response.parse(servletTester.getResponses(request.generate()));
        
        return response;
    }
    
    private String getMethodCallForRequestBody(String methodName)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\"?>");
        sb.append("<methodCall>");
        sb.append("<methodName>");
        sb.append(methodName);
        sb.append("</methodName>");
        sb.append("<params>");
        sb.append("</params>");
        sb.append("</methodCall>");
        return sb.toString();
    }
}
