package com.atlassian.xmlrpc;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;

public class BinderRequestProcessorFactoryFactoryTest extends TestCase
{
    public void testBinderRequestProcessorFactoryFactory() throws Exception
    {
        final TypeOne typeOne = new TypeOne();
        final TypeTwo typeTwo = new TypeTwo();
        final TypeThree typeThree = new TypeThree();
        
        final List services = Arrays.asList(typeOne, typeTwo, typeThree);
        final BinderRequestProcessorFactoryFactory factory = new BinderRequestProcessorFactoryFactory(services);
        
        assertEquals(typeOne, factory.getRequestProcessorFactory(TypeOne.class).getRequestProcessor(null));
        assertEquals(typeTwo, factory.getRequestProcessorFactory(TypeTwo.class).getRequestProcessor(null));
        assertEquals(typeThree, factory.getRequestProcessorFactory(TypeThree.class).getRequestProcessor(null));
    }
    
    private class TypeOne
    {
    }

    private class TypeTwo
    {
    }

    private class TypeThree
    {
    }
}
