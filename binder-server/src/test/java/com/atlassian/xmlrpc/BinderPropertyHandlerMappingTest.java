package com.atlassian.xmlrpc;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import junit.framework.TestCase;

public class BinderPropertyHandlerMappingTest extends TestCase
{
    public void testSupportsVoidReturnTypes()
    {
        assertTrue(new BinderPropertyHandlerMapping().isVoidMethodEnabled());
    }

    public void testMethodNamesAreRemapped() throws Exception
    {
        BinderPropertyHandlerMapping mapping = new BinderPropertyHandlerMapping();
        mapping.addHandler("myobject", MockServiceMethods.class);
        String[] methods = mapping.getListMethods();
        assertEquals(2, methods.length);
        assertEquals("myobject.GetShoppingCart", methods[0]);
        assertEquals("myobject.getMyObject", methods[1]);
    }
    
    public class MockServiceMethods implements MockServiceMethodInterface
    {
        public String getMyObject() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getSomethingElse() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
    
    public interface MockServiceMethodInterface
    {
        String getMyObject();
        
        @ServiceMethod("GetShoppingCart")
        String getSomethingElse();
    }
}