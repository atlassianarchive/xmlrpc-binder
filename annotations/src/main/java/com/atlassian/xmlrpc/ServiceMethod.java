package com.atlassian.xmlrpc;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Allows the remapping of remote method names
 * 
 * For example, a remote method name could be called "Deploy" but could be remapped
 * to the more Java friendly name "deploy"
 * 
 * @author <a href="mailto:james@atlassian.com">James William Dumay</a>
 */
@Target(ElementType.METHOD)
@Retention(value=RetentionPolicy.RUNTIME)
public @interface ServiceMethod
{
    /**
     * Name of the remote method
     * @return name
     */
    String value();
}
