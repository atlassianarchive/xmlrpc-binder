package com.atlassian.xmlrpc;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.util.Vector;
import org.apache.commons.codec.binary.Base64;
import redstone.xmlrpc.XmlRpcClient;
import redstone.xmlrpc.XmlRpcException;
import redstone.xmlrpc.XmlRpcFault;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * Binder implementation using the Redstone XML-RPC Client
 *
 * Only supports HTTP Basic Auth
 *
 * See {@link http://xmlrpc.sourceforge.net/}
 * 
 * @author jdumay
 */
public class RedstoneBinder implements Binder
{
    private static final String HTTP_AUTH_HEADER = "Authorization";

    public <T> T bind(Class<T> bindClass, URL url) throws BindingException
    {
        return bind(bindClass, url, null);
    }

    public <T> T bind(Class<T> bindClass, URL url, ConnectionInfo connectionInfo) throws BindingException
    {
        if (!bindClass.isInterface())
        {
            throw new BindingException("Class " + bindClass.getName() + "is not an interface");
        }
        ServiceObject serviceObject = bindClass.getAnnotation(ServiceObject.class);
        if (serviceObject == null)
        {
            throw new BindingException("Could not find ServiceObject annotation on " + bindClass.getName());
        }
        
        final XmlRpcClient client = getRpcClient(url, connectionInfo);
        XmlRpcInvocationHandler handler = new XmlRpcInvocationHandler(new XmlRpcClientProvider()
        {
            public Object execute(String serviceName, String methodName, Vector arguments) throws BindingException
            {
                try
                {
                    return client.invoke(serviceName + "." + methodName, arguments);
                }
                catch (XmlRpcFault e)
                {
                    throw new BindingException(e);
                }
                catch (XmlRpcException e)
                {
                    throw new BindingException(e);
                }
            }
        });
        
        return (T)Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { bindClass }, handler);
    }

    private XmlRpcClient getRpcClient(URL url, ConnectionInfo connectionInfo) throws BindingException
    {
        final XmlRpcClient client = new XmlRpcClient(url, false);
        if (connectionInfo != null && connectionInfo.getUsername() != null && connectionInfo.getPassword() != null)
        {
            client.setRequestProperty(HTTP_AUTH_HEADER, getAuthorizationHeaderValue(connectionInfo));
        }
        return client;
    }

    private String getAuthorizationHeaderValue(ConnectionInfo connectionInfo) throws BindingException
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Basic ");
        try
        {
            final byte[] bs = Base64.encodeBase64((connectionInfo.getUsername() + ":" + connectionInfo.getPassword()).getBytes("UTF-8"));
            sb.append(new String(bs, "UTF-8"));
        }
        catch (UnsupportedEncodingException e)
        {
            throw new BindingException(e);
        }
        return sb.toString();
    }
}
